package controller

import (
	"antrian/app/model"
	"fmt"
	"net/http"
	"time"

	//	"time"

	//	"github.com/FadhlanHawali/Digitalent-Kominfo_Introduction-MVC-Golang-Concept/app/model"
	"github.com/gin-gonic/gin"
)

func AddAntrianHandler(c *gin.Context) {
	flag, err := model.AddAntrian()
	if flag {
		c.JSON(http.StatusOK, map[string]interface{}{
			"status": "success",
		})
	} else {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status": "failed",
			"error":  err,
		})
	}
}

func GetAntrianHandler(c *gin.Context) {
	flag, err, resp := model.GetAntrian()
	if flag {
		c.JSON(http.StatusOK, map[string]interface{}{
			"status": "success",
			"data":   resp,
		})
	} else {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status": "failed",
			"error":  err,
		})
	}
}

func UpdateAntrianHandler(c *gin.Context) {
	idAntrian := c.Param("idAntrian")
	flag, err := model.UpdateAntrian(idAntrian)
	if flag {
		c.JSON(http.StatusOK, map[string]interface{}{
			"status": "success",
		})
	} else {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status": "failed",
			"error":  err,
		})
	}
}

func DeleteAntrianHandler(c *gin.Context) {
	idAntrian := c.Param("idAntrian")
	flag, err := model.DeleteAntrian(idAntrian)
	if flag {
		c.JSON(http.StatusOK, map[string]interface{}{
			"status": "success",
		})
	} else {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status": "failed",
			"error":  err,
		})
	}
}

func PageAntrianHandler(c *gin.Context) {
	flag, err, result := model.GetAntrian()
	//var currentAntrian map[string]interface{}

	/*for _, item := range result {
		if item != nil {
			currentAntrian = item
			break
		}
	}*/

	var newestTime int64
	var newestTime_id string
	for _, item := range result {
		if item != nil {
			fmt.Println(item["id"])
			if item["status"] == true {
				fmt.Println("status true")
				tmp, _ := time.Parse("RFCB22", item["updated"].(string))
				fmt.Println(tmp.Unix())
				if newestTime < tmp.Unix() {
					fmt.Println("time lebih besar")
					newestTime = tmp.Unix()
					newestTime_id = item["id"].(string)
					fmt.Println(newestTime_id, newestTime)
				} else {
					fmt.Println("belum masuk")
				}
			}
		}

		if flag && len(result) > 0 {
			c.HTML(http.StatusOK, "index.html", gin.H{
				//"antrian": currentAntrian["id"],
			})
		} else {
			c.JSON(http.StatusBadRequest, map[string]interface{}{
				"status": "failed",
				"error":  err,
			})
		}
	}
}
